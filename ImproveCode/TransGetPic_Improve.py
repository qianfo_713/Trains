import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from PIL import Image
import FileLoader
from torch.utils import data
import numpy as np
from ImproveCode import TransChange
import matplotlib.pyplot as plt
import TransNetHLoaderPlus
from ImproveCode import FileLoader_Improve
from ImproveCode import TransSavePic


def GetPic(Train=True):
    batch_size = 100
    TrainFileName = '../data/MNIST/raw/train.txt'
    TestFileName = '../data/MNIST/raw/test.txt'
    participantNum = 3
    NoisyDimonsion = 100
    ImgDimonsion = 784

# 公有矩阵：
    PublicMat = np.random.normal(0, 1, (ImgDimonsion + NoisyDimonsion, ImgDimonsion))

# 创建participantNum个私有矩阵:
    SelfMats = []
    for i in range(participantNum):
        selfMat = np.random.normal(0, 1, (NoisyDimonsion, ImgDimonsion))

        SelfMats.append(selfMat)
    # 测试的私有矩阵
    TestSelfMat = np.random.normal(0, 1, (NoisyDimonsion, ImgDimonsion))

    print("矩阵创建完毕")
    if Train:
        FilePlaces, labelSets = FileLoader_Improve.filePlace_loader(TrainFileName)
        print(labelSets)
    else:
        FilePlaces,labelSets = FileLoader_Improve.filePlace_loader(TestFileName)
    number = len(FilePlaces)
    print(number)

    for i in range(int(number / batch_size)):
        imgs = []
        st = i * batch_size
        end = (i + 1) * batch_size
        labels = []
        if end > number:
            end = number
        # 读取图片 把图片放进数组中 统一处理
        for k in range(st, end):
            imgPlace = FilePlaces[i]
            imgPlace = "../" + imgPlace
            # 获取到对应位置的图片 只是一个而已 而且是numpy
            img = FileLoader_Improve.Image_loader_NoTrans(imgPlace)
            # print("img初始是")

            # 从N*N 变成n²的一维向量
            img = img.reshape(-1)
            imgs.append(img)
            # 存放标签
            labels.append(labelSets[k])

        #     存放了一个batch的数据
        imgs = np.array(imgs)

        #   获取这个batch的噪音矩阵
        noisy = np.random.normal(0, 1, (end - st, NoisyDimonsion))
        # noisy = torch.from_numpy(noisy).float()
        # 获取是第几个人的数据


        # 转换得到矩阵变换之后的数据
        # print(imgs.shape)
        if Train==True:
            part = int(number / batch_size / participantNum)
            index = int(i / part)
            # print(index)
            SelfMat = SelfMats[index]
            endMat = TransChange.ChangePicH(imgs, noisy, PublicMat, SelfMat)
            # 保存对应的数据
            TransSavePic.SavePic(endMat, labels, st, index, 28)
        else:
            # 创建测试私有矩阵

            SelfMat = TestSelfMat
            endMat = TransChange.ChangePicH(imgs,TestNoisyMat=noisy,TestPublicMat=PublicMat,
                                            TestSelfMat=SelfMat,Train=False)
            # print(endMat)
            TransSavePic.SavePic(endMat,labels,st,participant=0,PicDimosion=28,Train=False)