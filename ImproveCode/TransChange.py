import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from skimage import io
from PIL import Image
import FileLoader
from torch.utils import data
import numpy as np
import matplotlib.pyplot as plt

# 只用来转换矩阵
def ChangePicH(Data,NoisyMat=None,PublicMat=None,SelfMat=None,TestNoisyMat=None,TestPublicMat=None,
                TestSelfMat=None,Train=True):
    root = ""
    PMat = None
    NMat = None
    SMat = None
    if Train==True:
        # root = "Result/"
        PMat = PublicMat
        NMat = NoisyMat
        SMat = SelfMat
    else:
        # root = "TestResult/"
        PMat = TestPublicMat
        NMat = TestNoisyMat
        SMat = TestSelfMat
    # 根据是测试集还是训练集 进行划分
    # root = root + str(participantNum)+"/"
    # print("转化中")
    # 把数据和噪音矩阵和在一一起
    imgChange = np.concatenate((Data,NMat),axis=1)

    step1Mat = np.matmul(imgChange,PMat)
    # 第二部分：私有矩阵和噪音矩阵

    step2Mat = np.matmul(NMat,SMat)
    EndMat = step1Mat + step2Mat
    #  返回的是numpy 中间所有量都是numpy
    return EndMat