import torch
from PIL import Image
import matplotlib.pyplot as plt
import torchvision
from skimage import io
import numpy as np
default_transform = torchvision.transforms.Compose([
    torchvision.transforms.ToTensor()
])

def filePlace_loader(fileName):
    f= open(fileName)
    line = f.readline()
    labelSets = []
    FilePlaces = []

    i = 0

    while line:
        filePlace = line.split()[0]
        FilePlaces.append(filePlace)

        label = line.split()[1]

        labelSets.append(label)
        i = i + 1
        line = f.readline()
    f.close()
    return FilePlaces,labelSets

def Image_loader(fileName,transform = default_transform):
    img = Image.open(fileName)
    img = transform(img).unsqueeze(0)[0][0]

    img = np.array(img)

    # 都是numpy
    return img

def Image_loader_NoTrans(fileName):
    img = io.imread(fileName)
    img = np.array(img)

    #都是numpy
    return img

def Npy_loader(filename):
    img = np.load(filename)
    return img