import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from skimage import io
from PIL import Image
import FileLoader
from torch.utils import data
import numpy as np
import matplotlib.pyplot as plt
from ImproveCode import FileLoader_Improve

class TrainsNetHLoaderPlus(data.Dataset):
    def __init__(self,fileName,loader,transform=None,Train=True):
        ImgPlace,LabelSet = FileLoader_Improve.filePlace_loader(fileName)
        size = len(LabelSet)

        LabelSet = [np.long(i) for i in LabelSet]

        labels = torch.from_numpy(np.array(LabelSet)).long()
        # print(labels)
        # print("转换成功")

        self.size =size
        self.labels = labels
        self.ImgPlaces = ImgPlace
        self.loader = loader
        self.transform = transform
        self.Train = Train

    def __getitem__(self, item):
        # print("我被循环了")
        img = self.loader(self.ImgPlaces[item])
        if self.transform is not None:
            img = self.transform(img)
        else:
            img = torch.from_numpy(np.array(img)).float()
        label = self.labels[item]
        # print(img)
        return img, label

    def __len__(self):
        return self.size

