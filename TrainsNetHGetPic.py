import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from PIL import Image
import FileLoader
from torch.utils import data
import numpy as np
import matplotlib.pyplot as plt
import TransNetHLoaderPlus

# mytransform = transforms.Compose([
#     transforms.ToTensor()
#     ]
# )
batch_size = 100
TrainFileName = 'data/MNIST/raw/train.txt'
TestFileName = 'data/MNIST/raw/test.txt'
participantNum = 3

noisyDimonsion = 50
ImgDimonsion = 784
# 公有矩阵：
PublicMat = np.random.normal(0,1, (ImgDimonsion + noisyDimonsion, ImgDimonsion))
PublicMat = torch.from_numpy(PublicMat).float()
# 创建participantNum个私有矩阵:
SelfMats = []
for i in range(participantNum):
      selfMat = np.random.normal(0,1,(noisyDimonsion,ImgDimonsion))
      selfMat = torch.from_numpy(selfMat).float()
      SelfMats.append(selfMat)
# 创建测试私有矩阵
TestMat = np.random.normal(0,1,(noisyDimonsion,ImgDimonsion))
TestMat = torch.from_numpy(TestMat).float()
print("矩阵创建完毕")

# 下面是将原始图像变成 修改后的图像 只用一遍即可。

TransNetHLoaderPlus.TrainsNetHChange(FileName=TrainFileName,NoisyDimonsion = noisyDimonsion,ImgDimonsion=ImgDimonsion,
                                                               PublicMat=PublicMat,SelfMats=SelfMats,participantNum=participantNum,
                                                               batch=batch_size,Train=True)

TransNetHLoaderPlus.TrainsNetHChange(FileName=TestFileName,NoisyDimonsion = noisyDimonsion,ImgDimonsion=ImgDimonsion,
                                                               PublicMat=PublicMat,SelfMats=SelfMats,participantNum=participantNum,
                                                               batch=batch_size,Train=False,TestMat=TestMat)
