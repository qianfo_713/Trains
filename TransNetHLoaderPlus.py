import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from skimage import io
from PIL import Image
import FileLoader
from torch.utils import data
import numpy as np
import matplotlib.pyplot as plt

def default_loader(path):
    return np.load(path)
def default_loader2(path):
    return Image.open(path)

def TrainsNetHChange(FileName = None,NoisyDimonsion = None,ImgDimonsion = None,PublicMat=None,SelfMats = None,participantNum = None,
                     batch = None,Train = True,TestMat = None,loader = default_loader2):

        FilePlaces,LabelSet = FileLoader.filePlace_loader(FileName)
        number = len(FilePlaces)


        # 将Label转换成tensor类型
        # 如果是训练集 就放到labels 是测试集就放到Testlabels
        if Train:
            LabelSet = [np.long(i) for i in LabelSet]
            np.save("labels.npy",LabelSet)
        else:
            LabelSet = [np.long(i) for i in LabelSet]
            np.save("Testlabels.npy", LabelSet)

        #  这里也是 如果是训练集就把读取位置改为result下 否则到TestResult下

        imgTotal = []
        imgTotal = torch.from_numpy(np.array(imgTotal)).float()
        for i in range(int(number/batch)):

            imgs=[]
            st = i*batch
            end =(i+1)*batch
            labels = []
            if end>number:
                end = number
            for k in range(st,end):
                imgPlace = FilePlaces[k]
                print(imgPlace)
                img = io.imread(imgPlace)
                img = torch.from_numpy(img)
                # print(img)
                img = img.float().view(-1)
                # print(img)
                img = np.array(img)
                # print(img.shape)
                imgs.append(img)
                # 标签的存储
                label = LabelSet[k]
                labels.append(label)

            #完成了批量图像的读取，现在需要转化了:
            imgs = torch.from_numpy(np.array(imgs)).float()

            noisy = np.random.normal(0,1,(end-st,NoisyDimonsion))
            noisy = torch.from_numpy(noisy).float()
            imgChange = torch.cat((imgs,noisy),1)

            step1Mat =  imgChange.mm(PublicMat)
            SelfMat = []
            # 第二部分：
            if Train:
                part = int(number/batch/participantNum)
                index  = int(i/part)
                SelfMat = SelfMats[index]
                output_dir = "result/"

            else:
                # TestMat = np.random.normal(0,1,(50,ImgDimonsion))
                # TestMat = torch.from_numpy(TestMat).float()
                SelfMat = TestMat
                output_dir = "TestResult/"

            step2Mat = noisy.mm(SelfMat)

            EndMat = step1Mat + step2Mat
            # EndMat = step1Mat
            # 大小为100*784 Tensor
            imgs = EndMat


            for pic_index in range(100):
                img = imgs[pic_index];
                # 转换到了28*28
                img = img.view(28,-1)
                img = img.numpy()
                # print("img是 ")
                # print(img)
                # np.save(output_dir+"ChangePic"+str(i*batch+pic_index),img)
                # print("存储成功")
                #
                im = Image.fromarray(img)
                im = im.convert("L")
                #
                im.save(output_dir+"ChangePic"+str(i*batch+pic_index)+".jpg")
                #
                # img_test = io.imread(output_dir+"ChangePic"+str(i*batch+pic_index)+".jpg")
                # print("img_out 是")
                # print(img_test)





class TrainsNetHLoaderPlus(data.Dataset):
    def __init__(self,transform = None,Train = True,loader = default_loader2):
        if Train:
            labels = np.load("labels.npy")
        else:
            labels = np.load("Testlabels.npy")
        size = len(labels)
        print(size)
        labels = torch.from_numpy(labels).long()

        self.Train = Train
        self.size = size
        self.labels = labels
        self.transform = transform
        self.loader = loader

    def __getitem__(self, item):
        if self.Train:
            root = "result/ChangePic"
        else:
            root = "TestResult/ChangePic"
        img = self.loader(root+str(item)+".jpg")
        if self.transform is not None:
            img = self.transform(img)
        # img = torch.from_numpy(img)
        label = self.labels[item]

        return img,label


    def __len__(self):
        return self.size
